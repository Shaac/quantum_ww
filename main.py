#!/usr/bin/python3

from itertools import permutations
from random import choice

class Quantum():
    """ This class is used to help the narrator in a Quantum Werewolf game. """

    def __init__(self, n):
        """ Create a game for n players. """
        self.states = list(self.getStates(n))
        self.n = n
        self.known = {player: 0 for player in range(n)}
        self.deads = []

    def __repr__(self):
        """ Display the array of probabilities. """
        line1 = "           Good Evil  Dead\n"
        line2 = "Player{:3}: {:3}% {:3}%  {:3}%"
        line3 = "Player{:3}: {:10} {:3}%"
        proba = [(int(good * 100), int(evil * 100), int(dead * 100)) for
                (good, evil, dead) in map(self.probabilities, range(self.n))]
        lines = (line3.format(p, self.roleToString(self.known[p]), proba[p][2])
                if self.known[p] else line2.format(p, *proba[p])
                for p in range(self.n))
        return line1 + "\n".join(lines)

    def attack(self, player, target):
        """
        Apply the effect of a player attacking a target.
        In any universe where the player is the dominant werewolf the target
        must be alive and non-wereweolf.
        """
        for state in self.states:
            alpha = min([x for x in state if x > 2])
            if state[player] == alpha:
                if state[target] > 2 or state[target] < 0: # Werewolf or dead
                    state[player] = 0 # We mark the state as to be removed
                else:
                    state[target] *= -1 # This kill the player
        # We remove the incompatible states:
        self.states[:] = [s for s in self.states if not s[player] == 0]

    def death(self, player, vote = True):
        """
        Kill a player, if vote is True, or just apply the effect of his death.
        Return the player's role.
        """
        # First we have to pick a role:
        if vote: # We pick a state where the player is still alive
            role = choice([s for s in self.states if s[player] > 0])[player]
        else: # We pick any state
            role = abs(choice(self.states)[player])
        # Then we destroy the incompatible states:
        if vote:
            self.states[:] = [s for s in self.states if s[player] == role]
        else:
            self.states[:] = [s for s in self.states if abs(s[player]) == role]
        # Finally, the player is dead in all the states:
        for state in self.states:
            state[player] = -role
        if not player in self.deads:
            self.deads.append(player)
        return role

    def probabilities(self, player):
        """ Gives the probabilities of state of on player. """
        good = evil = dead = 0
        for state in self.states:
            if abs(state[player]) > 2:
                evil += 1
            else:
                good += 1
            if state[player] < 0:
                dead += 1
        total = len(self.states)
        return (good / total, evil / total, dead / total)

    def vision(self, player, target):
        """
        Return the result of the player's vision of the target.
        -1 if the player can not be a seer, 1 is the vision is good, 0 if evil.
        """
        # We take the states where the player is seer:
        states = [s for s in self.states if s[player] == 2]
        if not len(states):
            return -1 # The player is not a alive seer in any universe
        else:
            isGood = abs(choice(states)[target]) < 3
            if isGood:
                cond = lambda s: s[player] == 2 and abs(s[target]) > 2
            else:
                cond = lambda s: s[player] == 2 and abs(s[target]) < 2
            self.states[:] = [s for s in self.states if not cond(s)]
            return 1 if isGood else 0

    def clean(self):
        """
        Kill the 100% dead players.
        Compute the role of the players with only one role.
        If the game is over, return the list of all possible ending states.
        If the game continues, return an empty list.
        """
        # We kill the 100% dead player with the death function, until it has no
        # effects:
        n_states = 0
        while n_states != len(self.states):
            n_states = len(self.states)
            for player in range(self.n):
                _, _, dead = self.probabilities(player)
                if dead == 1 and player not in self.deads:
                    self.death(player, False)
        # We look for known roles:
        for player in range(self.n):
            role = abs(self.states[0][player])
            if not len([s for s in self.states if abs(s[player]) != role]):
                self.known[player] = role
        # We see if the game is over:
        if max([i for i in self.states[0] if i < 3]) < 0: # Werewolves have won
            ret = self.states[:]
            self.states[:] = [choice(self.states)]
            return ret
        if max(self.states[0]) < 3: # Werewolves have lost
            ret = self.states[:]
            self.states[:] = [choice(self.states)]
            return ret
        return []

    @staticmethod
    def getStates(n):
        """
        Return the possible states for a n players game.
        One third of the players will be werwolves. And there will be one seer.
        """
        for roles in permutations(range(n), int(n / 3) + 1):
            state = [1 for _ in range(n)]
            for role, player in enumerate(roles):
                state[player] = role + 2
            yield state

    @staticmethod
    def roleToString(role):
        """ Convert a role as integer into a explicit string. """
        roles = {1: 'villager', 2: 'seer'}
        if role in roles:
            return roles[role]
        else:
            return "werewolf {}".format(role - 2)

    @staticmethod
    def stateToString(state):
        """ Return an explicit string to describe the state. """
        line = "Player{:3} is {}{}."
        return "\n".join(line.format(player, Quantum.roleToString(abs(role)),
            " (dead)" * (role < 0)) for player, role in enumerate(state))

class IOQuantum(Quantum):
    """ Use the Quantum class to play a quantum werewolves game, with IO. """

    def __init__(self):
        """ Asks for the number of players and constructs the game. """
        n = 0
        while n < 3:
            try:
                n = int(input("Number of players: "))
            except ValueError:
                n = 0
        Quantum.__init__(self, n)
        self.nights = 0

    def day(self):
        """
        Day turn: shows the probabilities, and asks for a dead.
        Return True if the game continues.
        """
        end = self.clean()
        if not len(end): # The game continues
            print(self)
            player = -1
            while player < 0 or player >= self.n or player in self.deads:
                try:
                    player = int(input("Lynched player: "))
                except ValueError:
                    player = -1
            role = self.death(player)
            print("The player was {}.".format(self.roleToString(role)))
            end = self.clean()
        if len(end): # The game ends
            self.end(end)
            return False
        return True

    def end(self, states):
        """ Ends the game with states as possible ends. """
        print("---")
        print("The game is over.")
        if len(states) > 1:
            print("Possible states:")
            print("---")
            for state in states:
                print(self.stateToString(state))
                print("---")
            print("And the chosen one is:")
        print(self.stateToString(self.states[0]))

    def night(self):
        """ Asks for attacks if not the first night. Asks for visions. """
        # Attacks:
        if self.nights:
            for player in range(self.n):
                if player not in self.deads:
                    target = -1
                    while target < 0 or target>=self.n or target in self.deads:
                        try:
                            target = int(input("Player{:3}'s attack: ".format(
                                player)))
                        except ValueError:
                            target = -1
                    self.attack(player, target)
        # Visions:
        for player in range(self.n):
            if player not in self.deads and not (
                    2 in self.known.values() and self.known[player] != 2):
                target = -1
                while target < 0 or target >= self.n:
                    try:
                        target = int(input("Player{:3}'s vision: ".format(
                            player)))
                    except ValueError:
                        target = -1
                vision = self.vision(player, target)
                if vision == -1:
                    print("No seer power.")
                elif vision == 1:
                    print("This is a good guy.")
                else:
                    print("This is a bad guy.")
                self.nights += 1

if __name__ == '__main__':
    game = IOQuantum()
    go = True
    while go:
        game.night()
        go = game.day()
    """
    print(game.vision(0,1))
    print(game)
    game.attack(0,1)
    print(game)
    print(game.death(1))
    for i in game.clean():
        print(game.stateToString(i))
    print(game)
    """
